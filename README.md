Tested setting a new module mapper path.
https://gcc.gnu.org/onlinedocs/gcc/C_002b_002b-Module-Mapper.html

The targets all23 and all17 demonstrate, that it compiles successfully
using -std=c++17 but not with c++20 and above.
https://gcc.gnu.org/bugzilla/show_bug.cgi?id=105512
