# Makefile

MAPPER_ROOT := mapper-root

MAPPERFLAGS = -fmodule-mapper='|@g++-mapper-server.exe -n -r '$(MAPPER_ROOT)''

all:
	rm -rf gcm.cache
	g++ -std=c++17 -fmodules-ts $(MAPPERFLAGS) -c -x c++-system-header string
	g++ -std=c++17 -fmodules-ts $(MAPPERFLAGS) -c -x c++-system-header iostream
	g++ -std=c++17 -fmodules-ts $(MAPPERFLAGS) -o test.o -c -x c++ test.cpp
	g++ -std=c++17 -fmodules-ts $(MAPPERFLAGS) -o main.o -c -x c++ main.cpp
	g++ -std=c++17 -fmodules-ts $(MAPPERFLAGS) -o main.exe main.o test.o

all23:
	rm -rf gcm.cache
	g++ -std=c++23 -fmodules-ts -c -x c++-system-header string
	g++ -std=c++23 -fmodules-ts -c -x c++-system-header iostream
	g++ -std=c++23 -fmodules-ts -o test.o -c -x c++ test.cpp
	g++ -std=c++23 -fmodules-ts -o main.o -c -x c++ main.cpp
	g++ -std=c++23 -fmodules-ts -o main.exe main.o test.o
	./main23.exe

all17:
	rm -rf gcm.cache
	g++ -std=c++17 -fmodules-ts -c -x c++-system-header string
	g++ -std=c++17 -fmodules-ts -c -x c++-system-header iostream
	g++ -std=c++17 -fmodules-ts -o test.o -c -x c++ test.cpp
	g++ -std=c++17 -fmodules-ts -o main.o -c -x c++ main.cpp
	g++ -std=c++17 -fmodules-ts -o main17.exe main.o test.o
	./main17.exe

#g++ -static-libgcc -static-libstdc++ -std=c++20 -fmodules-ts -o main.exe main.o test.o
